#pragma once
#include "Nucleus.h"

using namespace std;

class Ribosom
{
public:
	Protein* create_protein(string &RNA_transcript) const;
};
