#include "Ribosom.h"


////////////////////////////////////////////////////////////////////////////// RIBOSOM \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// this method creates a protein

Protein * Ribosom::create_protein(string & RNA_transcript) const
{
	Protein * list = new Protein;
	AminoAcidNode * node = nullptr;
	AminoAcid acid;
	string str[3];
	unsigned int start = 0;
	bool fine = true;
	list->init();
	for (start = 0; start + 3 < RNA_transcript.length() && fine; start = start + 3)
	{
		*str = RNA_transcript.substr(start, 3);
		acid = get_amino_acid(*str);
		if (acid == UNKNOWN)
		{
			list->clear();
			cerr << "invalid RNA transcript" << endl;
			system("pause");
			_exit(1);
			return nullptr;
		}

		if (list->get_first() == 0)
		{
			node = new AminoAcidNode;
			node->init(acid);
			list->set_first(node);
		}
		else
		{
			(*list).add(acid);
		}
	}
	return list;
}
