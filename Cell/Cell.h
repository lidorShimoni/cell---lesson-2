#pragma once

#include "Nucleus.h"
#include "Mitochondrion.h"

class Cell
{
private:
	Nucleus _nucleus;
	Ribosom _ribosom;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;

public:
	void init(const string dna_sequence, const Gene glucose_receptor_gene);

	bool get_ATP();

	// BONUS RELATED
	Nucleus get_nucleus();
};