#include "Nucleus.h"
#include "Ribosom.h"


using namespace std;


//this method initiallize the Gene
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
	{
		this->_start = start;
		this->_end = end;
		this->_on_complementary_dna_strand = on_complementary_dna_strand;
	}

		/* GETTER METHODS */

// GETTER METHOD - returns _start
unsigned int Gene::get_start() const
{
	return this->_start;
}

// GETTER METHOD - returns _end
unsigned int Gene::get_end() const
{
	return this->_end;
}

// GETTER METHOD - returns _on_complementary_dna_strand
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

		/* SETTER METHODS */

// SETTER METHOD - sets a new value to _start
void Gene::setStart(const unsigned int start)
{
	this->_start = start;
}

// SETTER METHOD - sets a new value to _end
void Gene::setEnd(const unsigned int end)
{
	this->_end = end;
}

// SETTER METHOD - sets a new value to _on_complementary_dna_strand
void Gene::setOn_complementary_dna_strand(const bool on_complementary_dna_strand) 
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}


////////////////////////////////////////////////////////////////////////////// NUCLEUS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// this method initiallize the nucleus
void Nucleus::init(const string dna_sequence)
{
	bool error = false;
	for (int i = 0; i < dna_sequence.length() && !error; i++)
	{
		if (dna_sequence[i] == 'G')
		{
			*this->_complementary_DNA_strand += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			*this->_complementary_DNA_strand += 'G';
		}
		else if (dna_sequence[i] == 'T')
		{
			*this->_complementary_DNA_strand += 'A';
		}
		else if (dna_sequence[i] == 'A')
		{
			*this->_complementary_DNA_strand += 'T';
		}
		else
		{
			cerr << "ERROR: Wrong nucleotide!" << endl;
			_exit(1);
		}
		*this->_DNA_strand += dna_sequence[i];
	}
}

		/* GETTER METHODS */

// GETTER METHOD - returns RNA
string Nucleus::get_RNA_transcript(const Gene & gene) const
{
	string rna = this->_DNA_strand->substr(gene.get_start(), gene.get_end());
	if (gene.get_end() > this->_DNA_strand->length())
	{
		cerr << "Error accured." << endl;
		exit(1);
	}
	if (gene.is_on_complementary_dna_strand())
	{
		rna = this->_complementary_DNA_strand->substr(gene.get_start(), gene.get_end());
		//rna = this->_complementary_DNA_strand->substr(g)
	}
	for (int i = 0; i < rna.length(); i++)
	{
		switch (rna[i])
		{
		case 'T':
			rna[i] = 'U';
			break;
		}
	}
	return rna;
}
 
// GETTER METHOD - returns reversed DNA
string Nucleus::get_reversed_DNA_strand() const
{
	string str;
	str = *this->_DNA_strand;
	reverse(str.begin(), str.end());
	return str;
}

// GETTER METHOD - returns number of code appearences in DNA
unsigned int Nucleus::get_num_of_codon_appearances(const string& codon) const
{
	int occurrences = 0;
	string::size_type pos = 0;
	while ((pos = (*this->_DNA_strand).find(codon, pos)) != string::npos) {
		++occurrences;
		pos += codon.length();
	}
	return occurrences;
}

string * Nucleus::get_dna()
{
	return this->_DNA_strand;
}

string * Nucleus::get_complementary_dna()
{
	return this->_complementary_DNA_strand;
}



