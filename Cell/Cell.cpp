#include "Cell.h"

////////////////////////////////////////////////////////////////////////////// CELL \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


// this method initializes all the classes
void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();

}

bool Cell::get_ATP()
{
	string transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein * protein = this->_ribosom.create_protein(transcript);
	if (!protein)
	{
		cerr << "Unable to create protein" << endl;
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*protein);
	if (this->_mitochondrion.produceATP())
	{
		this->_atp_units = 100;
		return true;
	}
	delete protein;
	return false;
}

Nucleus Cell::get_nucleus()
{
	return this->_nucleus;
}
