#pragma once
#include "Cell.h"
using namespace std;

class Virus
{
private:
	string _RNA_sequence;

public:
	void init(const std::string RNA_sequence);
	void infect_cell(Cell& cell) const;

};