#include "Mitochondrion.h"


////////////////////////////////////////////////////////////////////////// MITOCHONDRION \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// this method initializes the mitochodrion
void Mitochondrion::init()
{
	this->_glocuse_level = 50; // this is your mistake i had to do it/
	this->_has_glocuse_receptor = false;
}

// this method set the 
void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcidNode * curr = nullptr;
	enum AminoAcid arr[7] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END };
	bool fine = true;
	int i = 0;
	for (curr = protein.get_first(); curr != nullptr && fine; curr = curr->get_next())
	{
		if (curr->get_data() != arr[i])
		{
			fine = false;
		}
		i++;
	}
	this->_has_glocuse_receptor = fine;
}

// SETTER METHOD - this method sets the _glucose_level
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

// this method checkes if the mitochondrion can produce atp
bool Mitochondrion::produceATP() const
{
	return this->_has_glocuse_receptor && this->_glocuse_level >= 50 ? true : false;
}