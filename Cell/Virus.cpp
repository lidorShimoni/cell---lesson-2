#include "Virus.h"

void Virus::init(const std::string RNA_sequence)
{
	this->_RNA_sequence = RNA_sequence;
}

void Virus::infect_cell(Cell & cell) const
{
	string *virus_DNA = new string;
	
	*virus_DNA = this->_RNA_sequence;
	for (int i = 0; i < virus_DNA->length(); i++)
	{
		switch ((*virus_DNA)[i])
		{
		case 'T':
			(*virus_DNA)[i] = 'U';
			break;
		}
	}
	string *virus_C_DNA = new string;
	*virus_C_DNA = *virus_DNA;
	for (int i = 0; i < virus_C_DNA->length(); i++)
	{
		switch ((*virus_C_DNA)[i])
		{
		case 'G':
			(*virus_C_DNA)[i] = 'C';
			break;
		case 'A':
			(*virus_C_DNA)[i] = 'T';
			break;
		case 'C':
			(*virus_C_DNA)[i] = 'G';
			break;
		case 'T':
			(*virus_C_DNA)[i] = 'A';
			break;
		}
	}
	string *DNA = cell.get_nucleus().get_dna();
	string *C_DNA = cell.get_nucleus().get_complementary_dna();
	DNA->insert(DNA->length() / 2, *virus_DNA);
	C_DNA->insert(C_DNA->length() / 2, *virus_C_DNA);

	// stoped in the middle >_<
	
}
