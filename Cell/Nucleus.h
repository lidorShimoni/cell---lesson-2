#pragma once
#include "Protein.h"
#include "Ribosom.h"
#include <iostream>
#include <string>

using namespace std;


class Gene
{
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;

public:

	//this function initiallize the Gene
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	/* GETTER FUNCTIONS */
	// GETTER FUNCTION - returns _start
	unsigned int get_start() const;
	// GETTER FUNCTION - returns _end
	unsigned int get_end() const;
	// GETTER FUNCTION - returns _on_complementary_dna_strand
	bool is_on_complementary_dna_strand() const;

	/* SETTER FUNCTIONS */
	// SETTER FUNCTION - sets a new value to _start
	void setStart(const unsigned int start);
	// SETTER FUNCTION - sets a new value to _end
	void setEnd(const unsigned int end);
	// SETTER FUNCTION - sets a new value to _on_complementary_dna_strand
	void setOn_complementary_dna_strand(const bool on_complementary_dna_strand);
};

class Nucleus
{
private:
	string* _DNA_strand = new string;
	string* _complementary_DNA_strand = new string;

public:

	// this function initiallize the Gene
	void init(const string dna_sequence);

	// GETTER FUNCTION - returns 
	string get_RNA_transcript(const Gene& gene) const;

	// GETTER FUNCTION - returns reversed DNA
	string get_reversed_DNA_strand() const;

	// GETTER FUNCTION - returns number of code appearences in DNA
	unsigned int get_num_of_codon_appearances(const string& codon) const;

	// BONUS RELATED ---------------------------------------------------------
	string * get_dna();
	string * get_complementary_dna();

};
